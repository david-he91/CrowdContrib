1. MindSpore 计算算子常见问题FAQ.pdf
该文档包含计算算子开发过程中所需的背景知识及常见问题。

2. MindSpore Ascend计算算子接入指南.pdf
该文档包含Ascend计算算子开发所需要了解的所有知识点，用于指导算子开发人员进行计算算子开发。

3. MindSpore Ascend计算算子接入开发规范.pdf
该文档包含需要交付的功能特性，算子开发人员需要遵守的编码规范、接口规范、命名规范、目录规范等，用于帮助算子开发人员规范开发过程，通过代码评审。

4. MindSpore Ascend计算算子接入开发交付件.pdf
该文档列举了计算算子开发包含的所有交付件，是对算子开发规范的提炼，用于帮助开发人员整理交付件。

5. MindSpore 计算算子测试验收指标说明.pdf
该文档包含从测试的角度描述的测试验收时关注的问题点，包含接口描述、功能、精度及合法性检查等，用于帮助开发人员了解如何通过测试验收。

6. MindSpore 计算算子测试报告.xlsx
该表格包含测试验收需要核验的内容，是对算子测试验收指标说明的提炼，用于帮助开发人员进行开发后的算子自测。

7. MindSpore Ascend计算算子接入设计文档-参考示例.docx
该文档包含了Ascend计算算子：Atanh的算子设计，以及BCELoss的nn接口设计，为最终交付件之一的参考示例。

8. MindSpore Ascend计算算子接入设计文档-模板.doc
该文档为交付件之一的模板，一个算子需要对应一个算子设计文档，算子开发人员需要在此模板基础上根据算子实际情况进行算子设计。

9. MindSpore 算子众智Gitee开发者使用指南.pdf
该文档包含了本次算子众智项目的开发模式以及大致开发流程，用于帮助开发团队完成从代码开发到代码交付的整个过程。